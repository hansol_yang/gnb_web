var webpack = require('webpack');
var path = require('path');


module.exports = {
    entry: [
        path.join(__dirname, 'front', 'index.js'),
        'webpack-dev-server/client?http://localhost:3001',
        'webpack/hot/only-dev-server'
    ],

    output: {
        path: '/',
        filename: 'bundle.js'
    },

    devServer: {
        hot: true,
        inline: true,
        historyApiFallback: true,
        publicPath: '/',
        contentBase: path.join(__dirname, 'back', 'public'),
        filename: 'bundle.js',
        proxy: {
            '**': 'http://localhost:3000'
        }
    },

    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot-loader', 'babel-loader?' + JSON.stringify({
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                })],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }
        ]
    }
};
