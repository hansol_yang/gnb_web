var webpack = require('webpack');
var path = require('path');


module.exports = {
    entry: [
        path.join(__dirname, 'front', 'index.js')
    ],

    output: {
        path: path.join(__dirname, 'back', 'public'),
        filename: 'bundle.js'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }
        ]
    }
};
