import update from 'react-addons-update';

import * as types from '../actions/comments/ActionTypes';


const initialState = {
    status: 'INIT'
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CREATE_COMMENT:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.CREATE_COMMENT_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'}
            });

        case types.CREATE_COMMENT_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default commentsReducer;