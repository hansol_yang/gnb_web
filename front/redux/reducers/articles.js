import update from 'react-addons-update';

import * as types from '../actions/articles/ActionTypes';


const initialState = {
    status: 'INIT',
    response: {},
    articles: [],
    article: {},
    recents: []
};

const articlesReducer = (state = initialState, action) => {
    window.scrollTo(0, 0);
    switch (action.type) {
        case types.CREATE:
        case types.READ_ALL_BY_OPTION:
        case types.READ_RECENTS:
        case types.READ_BY_ID:
        case types.DELETE:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.CREATE_SUCCESS:
        case types.UPDATE_SUCCESS:
        case types.DELETE_SUCCESS:
            if (action.response.msg === '게시물 생성 권한이 없습니다.' && action.response.code === 1) {
                alert('PERMISSION DENIED!!!');
                localStorage.clear();
                location.href = '/';
            }
            return update(state, {
                status: {$set: 'SUCCESS'},
                response: {$set: action.response}
            });

        case types.READ_ALL_BY_OPTION_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                articles: {$set: action.articles},
                response: {$set: action.response}
            });

        case types.READ_RECENTS_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                recents: {$set: action.recents},
                response: {$set: action.response}
            });

        case types.READ_BY_ID_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                article: {$set: action.article},
                response: {$set: action.response}
            });

        case types.CREATE_FAILURE:
        case types.READ_ALL_BY_OPTION_FAILRE:
        case types.READ_RECENTS_FAILURE:
        case types.READ_BY_ID_FAILURE:
        case types.DELETE_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default articlesReducer;