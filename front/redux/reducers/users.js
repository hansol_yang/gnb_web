import update from 'react-addons-update';

import * as types from '../actions/users/ActionTypes';


const initialState = {
    status: 'INIT',
    response: {}
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SIGNUP:
        case types.SIGNIN:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.SIGNUP_SUCCESS:
        case types.SIGNIN_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                response: {$set: action.response}
            });

        case types.SIGNUP_FAILURE:
        case types.SIGNIN_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default usersReducer;