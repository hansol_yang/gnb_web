import {combineReducers} from 'redux';

import articlesReducer from './articles';
import usersReducer from './users';
import commentsReducer from './comments';


const rootReducer = combineReducers({
    articlesReducer,
    usersReducer,
    commentsReducer
});


export default rootReducer;