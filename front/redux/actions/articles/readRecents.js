import axios from 'axios';

import {READ_RECENTS, READ_RECENTS_SUCCESS, READ_RECENTS_FAILURE} from './ActionTypes';


function readRecents() {
    return {
        type: READ_RECENTS
    }
}

function readRecentsSuccess(response, recents) {
    return {
        type: READ_RECENTS_SUCCESS,
        response: response,
        recents: recents
    }
}

function readRecentsFailure() {
    return {
        type: READ_RECENTS_FAILURE
    }
}

export function readRecentsRequest() {
    return function(dispatch) {
        dispatch(readRecents());

        return axios({
            method: 'get',
            url: '/articles/recents'
        }).then(function(response) {
            dispatch(readRecentsSuccess(response.data, response.data.recents));
        }).catch(function(error) {
            dispatch(readRecentsFailure());
        })
    }
}