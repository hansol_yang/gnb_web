import axios from 'axios';

import {CREATE, CREATE_SUCCESS, CREATE_FAILURE} from './ActionTypes';


function _create() {
    return {
        type: CREATE
    }
}

function _createSuccess(response) {
    return {
        type: CREATE_SUCCESS,
        response: response
    }
}

function _createFailure() {
    return {
        type: CREATE_FAILURE
    }
}

export function _createRequest(title, body, option) {
    return function (dispatch) {
        dispatch(_create());

        return axios({
            method: 'post',
            url: '/articles',
            data: {
                title: title,
                body: body,
                option: option
            },
            headers: {'x-access-token': localStorage.getItem('token')}
        }).then(function (response) {
            dispatch(_createSuccess(response.data));
        }).catch(function (error) {
            console.log(error);
            dispatch(_createFailure());
        })
    }
}