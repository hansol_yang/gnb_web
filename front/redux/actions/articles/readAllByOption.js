import axios from 'axios';

import {READ_ALL_BY_OPTION, READ_ALL_BY_OPTION_SUCCESS, READ_ALL_BY_OPTION_FAILRE} from './ActionTypes';


function readAllByOption() {
    return {
        type: READ_ALL_BY_OPTION
    }
}

function readAllByOptionSuccess(response, articles) {
    return {
        type: READ_ALL_BY_OPTION_SUCCESS,
        response: response,
        articles: articles
    }
}

function readAllByOptionFailure() {
    return {
        type: READ_ALL_BY_OPTION_FAILRE
    }
}

export function readAllByOptionRequest(option) {
    return function(dispatch) {
        dispatch(readAllByOption());

        return axios({
            method: 'get',
            url: `/articles/option/${option}`
        }).then(function(response) {
            dispatch(readAllByOptionSuccess(response.data, response.data.articles));
        }).catch(function(error) {
            dispatch(readAllByOptionFailure());
        })
    }
}