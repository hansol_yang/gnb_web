import axios from 'axios';

import {UPDATE, UPDATE_SUCCESS, UPDATE_FAILURE} from './ActionTypes';


function update() {
    return {
        type: UPDATE
    }
}

function updateSuccess(response) {
    return {
        type: UPDATE_SUCCESS,
        response: response
    }
}

function updateFailure() {
    return {
        type: UPDATE_FAILURE
    }
}

export function updateRequest(id, title, body, option) {
    return function (dispatch) {
        dispatch(update());

        return axios({
            method: 'put',
            url: `/articles/${id}`,
            data: {title, body, option},
            headers: {'x-access-token': localStorage.getItem('token')}
        }).then(function(response) {
            dispatch(updateSuccess(response.data));
        }).catch(function(error) {
            dispatch(updateFailure());
        })
    }

}