import axios from 'axios';

import {DELETE, DELETE_SUCCESS, DELETE_FAILURE} from './ActionTypes';


function _delete() {
    return {
        type: DELETE
    }
}

function _deleteSuccess(response) {
    return {
        type: DELETE_SUCCESS,
        response: response
    }
}

function _deleteFailure() {
    return {
        type: DELETE_FAILURE
    }
}

export function _deleteRequest(id) {
    return function(dispatch) {
        dispatch(_delete());

        return axios({
            method: 'delete',
            url: `/articles/${id}`,
            headers: {'x-access-token': localStorage.getItem('token')}
        }).then(function(response) {
            dispatch(_deleteSuccess(response.data));
        }).catch(function(error) {
            dispatch(_deleteFailure());
        })

    }
}