import axios from 'axios';

import {READ_BY_ID, READ_BY_ID_SUCCESS, READ_BY_ID_FAILURE} from './ActionTypes';


function readById() {
    return {
        type: READ_BY_ID
    }
}

function readByIdSuccess(response, article) {
    return {
        type: READ_BY_ID_SUCCESS,
        response: response,
        article: article
    }
}

function readByIdFailure() {
    return {
        type: READ_BY_ID_FAILURE
    }
}

export function readByIdRequest(id) {
    return function (dispatch) {
        dispatch(readById());

        return axios({
            method: 'get',
            url: `/articles/${id}`
        }).then(function (response) {
            dispatch(readByIdSuccess(response.data, response.data.article));
        }).catch(function (error) {
            dispatch(readByIdFailure());
        })
    }

}