import axios from 'axios';

import {SIGNIN, SIGNIN_SUCCESS, SIGNIN_FAILURE} from './ActionTypes';


function signin() {
    return {
        type: SIGNIN
    }
}

function signinSuccess(response) {
    return {
        type: SIGNIN_SUCCESS,
        response: response
    }
}

function signinFailure() {
    return {
        type: SIGNIN_FAILURE
    }
}

export function signinRequest(username, password) {
    return function (dispatch) {
        dispatch(signin());

        return axios({
            method: 'post',
            url: '/users/signin',
            data: {username, password}
        }).then(function (response) {
            if(response.data.code === 300) {
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('isAdmin', response.data.isAdmin);
                localStorage.setItem('username', response.data.username);
                history.back();
            }
            dispatch(signinSuccess(response.data))
        }).catch(function (error) {
            dispatch(signinFailure())
        });
    }
}