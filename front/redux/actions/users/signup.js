import axios from 'axios';

import {SIGNUP, SIGNUP_SUCCESS, SIGNUP_FAILURE} from './ActionTypes';


function signup() {
    return {
        type: SIGNUP
    }
}

function signupSuccess(response) {
    return {
        type: SIGNUP_SUCCESS,
        response: response
    }
}

function signupFailure() {
    return {
        type: SIGNUP_FAILURE
    }
}

export function signupRequest(username, password) {
    return function (dispatch) {
        dispatch(signup());

        return axios({
            method: 'post',
            url: '/users/signup',
            data: {username, password}
        }).then(function (response) {
            dispatch(signupSuccess(response.data));
        }).catch(function (error) {
            dispatch(signupFailure())
        });
    }
}