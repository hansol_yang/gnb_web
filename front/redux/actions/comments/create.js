import axios from 'axios';

import {CREATE_COMMENT, CREATE_COMMENT_SUCCESS, CREATE_COMMENT_FAILURE} from './ActionTypes';


function createComment() {
    return {
        type: CREATE_COMMENT
    }
}

function createCommentSuccess() {
    return {
        type: CREATE_COMMENT_SUCCESS
    }
}

function createCommentFailure() {
    return {
        type: CREATE_COMMENT_FAILURE
    }
}

export function createCommentRequest(id, content) {
    return function(dispatch) {
        dispatch(createComment());

        return axios({
            method: 'post',
            url: `/comments/${id}`,
            data: {content},
            headers: {'x-access-token': localStorage.getItem('token')}
        }).then(function(response) {
            dispatch(createCommentSuccess());
        }).catch(function(error) {
            dispatch(createCommentFailure());
        })
    }
}