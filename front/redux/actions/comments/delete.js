import axios from 'axios';

import {DELETE_COMMENT, DELETE_COMMENT_SUCCESS, DELETE_COMMENT_FAILURE} from './ActionTypes';


function deleteComment() {
    return {
        type: DELETE_COMMENT
    }
}

function deleteCommentSuccess() {
    return {
        type: DELETE_COMMENT_SUCCESS
    }
}

function deleteCommentFailure() {
    return {
        type: DELETE_COMMENT_FAILURE
    }
}

export function deleteCommentRequest(id, commentId) {
    return function(dispatch) {
        dispatch(deleteComment());

        return axios({
            method: 'delete',
            url: `/comments/${id}`,
            data: {
                id: commentId
            }
        }).then(function(response) {
            dispatch(deleteCommentSuccess());
        }).catch(function(error) {
            dispatch(deleteCommentFailure());
        })
    }
}