import React from 'react';
import {connect} from 'react-redux';
import {ListItem} from 'material-ui/List'
import Link from 'react-router-dom/Link';

import List from './utils/List';
import CreateButton from './utils/CreateButton';
import PostingForm from './utils/PostingForm';
import {readAllByOptionRequest} from '../redux/actions/articles/readAllByOption';


class Post extends React.Component {
    constructor(props) {
        super(props);

        this._handleAfterPosting = this._handleAfterPosting.bind(this);
    }

    _handleAfterPosting() {
        this.props.handlePostingMode(false);
        this.props.readAllByOptionRequest(0);
    }

    componentDidMount() {
        this.props.readAllByOptionRequest(0);
    }

    componentWillUnmount() {
        this.props.handlePostingMode(false);
    }

    render() {
        return (
            <div className="container post">
                <p className="header">일반 게시판</p>
                {localStorage.getItem('token') ? undefined :
                    <Link to="/login" style={{textDecoration: 'none'}}>
                        <ListItem
                            primaryText="새로운 글 작성을 원하시면 로그인해주세요"
                            style={{textAlign: 'center', fontFamily: 'main'}}
                        />
                    </Link>
                }
                {this.props.isPosting ?
                    <PostingForm toggleFunc={this._handleAfterPosting}/> : this.props.posts.map((data, i) => (
                        <List
                            data={data}
                            key={i}
                        />
                    ))}
                {this.props.isPosting || !localStorage.getItem('token') ? undefined :
                    <CreateButton toggleFunc={this.props.handlePostingMode}/>}
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        posts: state.articlesReducer.articles
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readAllByOptionRequest: (option) => dispatch(readAllByOptionRequest(option))
    }
};

Post = connect(mapStateToProps, mapDispatchToProps)(Post);


export default Post;