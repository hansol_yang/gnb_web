import React from 'react';
import IconButton from 'material-ui/IconButton';
import IconLike from 'material-ui/svg-icons/action/favorite-border';
import IconMore from 'material-ui/svg-icons/navigation/more-vert';
import IconEdit from 'material-ui/svg-icons/editor/mode-edit';
import IconDelete from 'material-ui/svg-icons/action/delete'
import Divider from 'material-ui/Divider';
import {connect} from 'react-redux';
import MenuItem from 'material-ui/MenuItem';

import MoreFunc from './utils/MoreFunc';
import EditingForm from './utils/EditingForm';
import {readByIdRequest} from '../redux/actions/articles/readById';
import {updateRequest} from '../redux/actions/articles/update';
import {_deleteRequest} from '../redux/actions/articles/delete';
import {createCommentRequest} from '../redux/actions/comments/create';
import {deleteCommentRequest} from '../redux/actions/comments/delete';
import Comment from './utils/Comment';
import CommentInput from './utils/CommentInput';


class Detail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            popOverOpen: false
        };

        this._handlePopOver = this._handlePopOver.bind(this);
        this._handlePopOverClose = this._handlePopOverClose.bind(this);
        this._handleDelete = this._handleDelete.bind(this);
        this._handleAfterEdit = this._handleAfterEdit.bind(this);
        this._handleSubmitComment = this._handleSubmitComment.bind(this);
        this._handleDeleteComment = this._handleDeleteComment.bind(this);
    }

    _handlePopOver(e) {
        this.setState({
            anchorEl: e.currentTarget,
            popOverOpen: true
        })
    }

    _handlePopOverClose() {
        this.setState({
            popOverOpen: false
        })
    }

    _handleDelete() {
        if (confirm('정말 삭제하시나요 ?')) {
            this.props._deleteRequest(this.props.data._id);
            history.back();
        }
    }

    _handleAfterEdit() {
        this.props.handleEditingMode(false);
        this.props.readByIdRequest(this.props.match.location.state._id);
    }

    _handleSubmitComment(content) {
        this.props.createCommentRequest(this.props.data._id, content);
        this.props.readByIdRequest(this.props.match.location.state._id);
    }

    _handleDeleteComment(commentId) {
        if (confirm('정말 삭제하시나요?')) {
            this.props.deleteCommentRequest(this.props.data._id, commentId);
            this.props.readByIdRequest(this.props.match.location.state._id);
        }
    }

    componentDidMount() {
        this.props.readByIdRequest(this.props.match.location.state._id);
        this.props.handleDetailMode(true);
    }

    componentDidUpdate() {
        if (!this.props.isDetail) {
            history.back();
        }
    }

    render() {
        return (
            <div className="container detail">
                {this.props.isEditing ?
                    <EditingForm
                        data={this.props.data}
                        updateRequest={this.props.updateRequest}
                        toggleFunc={this._handleAfterEdit}
                    /> :
                    <section>
                        <section className="tool">
                            {localStorage.getItem('username') === this.props.data.username ?
                                <div>
                                    <div className="mobile">
                                        <IconButton
                                            onClick={this._handlePopOver}
                                        >
                                            <IconMore/>
                                        </IconButton>
                                    </div>
                                    <div className="computer">
                                        <MenuItem
                                            className="moreFunc"
                                            primaryText="수정"
                                            leftIcon={
                                                <IconEdit/>
                                            }
                                            onTouchTap={this._handlePopOverClose}
                                            onClick={() => this.props.handleEditingMode(true)}
                                        />
                                        <MenuItem
                                            className="moreFunc"
                                            primaryText="삭제"
                                            leftIcon={
                                                <IconDelete/>
                                            }
                                            onTouchTap={this._handlePopOverClose}
                                            onClick={() => this._handleDelete()}
                                        />
                                    </div>
                                </div>
                                : undefined
                            }
                        </section>
                        <section className="detailTitle">
                            <p>
                                {this.props.data.title}
                            </p>
                            <p>
                                by&nbsp;
                                {this.props.data.username}
                            </p>
                            <Divider/>
                        </section>
                        <section className="detailBody">
                            <p>
                                {this.props.data.body}
                            </p>
                        </section>
                        <MoreFunc
                            open={this.state.popOverOpen}
                            anchorEl={this.state.anchorEl}
                            handleCloseFunc={this._handlePopOverClose}
                            handleEditMode={this.props.handleEditingMode}
                            handleDelete={this._handleDelete}
                        />
                        <section className="detailComment">
                            <p>댓글&nbsp;</p>
                            <p>{this.props.data.comments ? this.props.data.comments.length : 0}</p>
                            <CommentInput handleSubmit={this._handleSubmitComment}/>
                                {this.props.data.comments ? this.props.data.comments.map((comment, i) => (
                                    <Comment key={i} comment={comment} handleDelete={this._handleDeleteComment}/>
                                )) : undefined}
                        </section>
                    </section>
                }
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        data: state.articlesReducer.article
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readByIdRequest: (id) => dispatch(readByIdRequest(id)),
        updateRequest: (id, title, body, option) => dispatch(updateRequest(id, title, body, option)),
        _deleteRequest: (id) => dispatch(_deleteRequest(id)),
        createCommentRequest: (id, content, username) => dispatch(createCommentRequest(id, content, username)),
        deleteCommentRequest: (id, commentId) => dispatch(deleteCommentRequest(id, commentId))
    }
};

Detail = connect(mapStateToProps, mapDispatchToProps)(Detail);


export default Detail;