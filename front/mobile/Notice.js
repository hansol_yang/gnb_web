import React from 'react';
import {connect} from 'react-redux';
import Link from 'react-router-dom/Link';
import {ListItem} from 'material-ui/List';

import List from './utils/List';
import CreateButton from './utils/CreateButton';
import PostingForm from './utils/PostingForm';
import {readAllByOptionRequest} from '../redux/actions/articles/readAllByOption';


class Notice extends React.Component {
    constructor(props) {
        super(props);

        this._handleAfterPosting = this._handleAfterPosting.bind(this);
    }

    _handleAfterPosting() {
        this.props.handlePostingMode(false);
        this.props.readAllByOptionRequest(1);
    }

    componentDidMount() {
        this.props.readAllByOptionRequest(1);
    }

    componentWillUnmount() {
        this.props.handlePostingMode(false);
    }

    render() {
        return (
            <div className="container notice">
                <p className="header">공지 사항</p>
                {localStorage.getItem('token') ? undefined :
                    <Link to="/login" style={{textDecoration: 'none'}}>
                        <ListItem
                            primaryText="새로운 글 작성을 원하시면 로그인해주세요"
                            style={{textAlign: 'center', fontFamily: 'main'}}
                        />
                    </Link>
                }
                {this.props.isPosting ? <PostingForm toggleFunc={this._handleAfterPosting}/> : this.props.notices.map((data, i) => (
                    <List
                        data={data}
                        key={i}
                    />
                ))}
                {this.props.isPosting || !JSON.parse(localStorage.getItem('isAdmin')) ? undefined : <CreateButton toggleFunc={this.props.handlePostingMode}/>}
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        notices: state.articlesReducer.articles
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readAllByOptionRequest: (option) => dispatch(readAllByOptionRequest(option))
    }
};

Notice = connect(mapStateToProps, mapDispatchToProps)(Notice);


export default Notice;