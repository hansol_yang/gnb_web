import React from 'react';
import {connect} from 'react-redux';

import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import Toast from './utils/Toast';


class Auth extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isNew: false, // 'true'면 회원가입
            toastOpen: false
        };

        this._handleAuthMode = this._handleAuthMode.bind(this);
    }

    _handleAuthMode() {
        this.setState({
            isNew: !this.state.isNew
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            toastOpen: true
        })
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div className="container auth">
                {this.state.isNew ?
                    <SignUp handleAuthMode={this._handleAuthMode}/>
                    : <SignIn handleAuthMode={this._handleAuthMode}/>
                }
                <Toast
                    open={this.state.toastOpen}
                    msg={this.props.usersResponse.msg || ''}
                />
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        usersResponse: state.usersReducer.response
    }
};

Auth = connect(mapStateToProps)(Auth);


export default Auth;