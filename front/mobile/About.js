import React from 'react';

import Paper from './utils/Paper';


class About extends React.Component {
    render() {
        return (
            <div className="container about">
                <Paper
                    title="지앤비란?"
                    body="지앤비는 알 지(知) 와 날 비(飛) 의 합성어입니다."
                />
                <Paper
                    title="활동 목적"
                    body="누구나 웹 프로그래밍에 있어서 자신감을 가질 수 있게 하는 것을 목적으로 하고 있습니다."
                />
                <Paper
                    title="현 회장"
                    body={
                        <img src="/images/jonmot.jpeg" style={{width: '100%'}} />
                    }
                />
                <Paper
                    title="릴리즈"
                    body="현재 동아리 작품이 없습니다. 일 좀 주세요 회장님"
                />
            </div>
        );
    }
}


export default About;