import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import SubmitButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';

import {_createRequest} from '../../redux/actions/articles/create';


class PostingForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            body: '',
            disable: true
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleEnter = this._handleEnter.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });

        if(this.state.title && this.state.body) {
            this.setState({
                disable: false
            })
        }

        if(!this.state.title || !this.state.body) {
            this.setState({
                disable: true
            })
        }
    }

    _handleSubmit() {
        if (this.state.title !== '' && this.state.body !== '') {
            switch (window.location.pathname) {
                case '/post':
                    this.props._createRequest(this.state.title, this.state.body, 0);
                    this.props.toggleFunc();
                    break;

                case '/notice':
                    this.props._createRequest(this.state.title, this.state.body, 1);
                    this.props.toggleFunc();
                    break;

                default:
                    return;
            }
        } else if(this.state.title === '') {
            alert('제목이 비었습니다.');
        } else if(this.state.body === '') {
            alert('내용이 비었습니다.');
        }
    }

    _handleEnter(e) {
        if (e.keyCode === 13) {
            this._handleSubmit();
        }
    }

    render() {
        return (
            <Paper zDepth={1} className="FormWrapper">
                <TextField
                    className="postingForm"
                    name="title"
                    floatingLabelText="제목"
                    fullWidth={true}
                    onChange={this._handleChange}
                    value={this.state.title}
                />
                <TextField
                    className="postingForm"
                    name="body"
                    floatingLabelText="내용"
                    style={{marginBottom: '2rem'}}
                    rows={7}
                    multiLine={true}
                    fullWidth={true}
                    onChange={this._handleChange}
                    value={this.state.body}
                    onKeyUp={this._handleEnter}
                />
                <SubmitButton
                    label="작성"
                    labelColor="white"
                    labelStyle={{fontFamily: 'main'}}
                    backgroundColor="black"
                    fullWidth={true}
                    onClick={this._handleSubmit}
                    disabled={this.state.disable}
                />
            </Paper>
        );
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        _createRequest: (title, body, option) => dispatch(_createRequest(title, body, option))
    }
};

PostingForm = connect(undefined, mapDispatchToProps)(PostingForm);


export default PostingForm;