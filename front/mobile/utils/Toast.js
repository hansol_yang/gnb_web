import React from 'react';
import Snackbar from 'material-ui/Snackbar';


const Toast = ({open, msg}) => (
    <Snackbar
        open={open}
        message={msg}
        autoHideDuration={2500}
    />
);


export default Toast;