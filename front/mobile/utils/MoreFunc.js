import React from 'react';
import PopOver from 'material-ui/Popover';
import MenuItem from 'material-ui/MenuItem';
import IconEdit from 'material-ui/svg-icons/editor/mode-edit';
import IconDelete from 'material-ui/svg-icons/action/delete'


const MoreFunc = ({open, anchorEl, handleCloseFunc, handleEditMode, handleDelete}) => (
    <PopOver
        open={open}
        anchorEl={anchorEl}
        onRequestClose={handleCloseFunc}
    >
        <MenuItem
            className="moreFunc"
            primaryText="수정"
            leftIcon={
                <IconEdit/>
            }
            onTouchTap={handleCloseFunc}
            onClick={() => handleEditMode(true)}
        />
        <MenuItem
            className="moreFunc"
            primaryText="삭제"
            leftIcon={
                <IconDelete/>
            }
            onTouchTap={handleCloseFunc}
            onClick={() => handleDelete()}
        />
    </PopOver>
);


export default MoreFunc;