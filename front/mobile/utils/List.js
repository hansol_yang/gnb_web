import React from 'react';
import {ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import Link from 'react-router-dom/Link';


const List = ({data}) => (
    <Link
        to={{
            pathname: '/detail',
            state: {
                _id: data._id
            }
        }}
        style={{textDecoration: 'none'}}
    >
        <ListItem
            className="list"
            leftAvatar={
                <Avatar>{data.username[0]}</Avatar>
            }
            primaryText={data.title}
            secondaryText={data.username}
        />
        <Divider/>
    </Link>
);



export default List;