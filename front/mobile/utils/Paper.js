import React from 'react';
import MuiPaper from 'material-ui/Paper';


const Paper = ({title, body}) => (
    <MuiPaper zDepth={1} className="paper">
        <p>{title}</p>
        <p>{body}</p>
    </MuiPaper>
);


export default Paper;