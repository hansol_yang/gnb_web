import React from 'react';
import TextField from 'material-ui/TextField';
import {ListItem} from 'material-ui/List';
import Link from 'react-router-dom/Link';
import Avatar from 'material-ui/Avatar';


class CommentInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            content: ''
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleEnter = this._handleEnter.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleEnter(e) {
        if(e.keyCode === 13) {
            this._handleSubmit();
        }
    }

    _handleSubmit() {
        if(this.state.content === '') {
            alert('댓글 내용을 입력해주세요.');
        } else {
            this.props.handleSubmit(this.state.content);
            this.setState({
                content: ''
            })
        }
    }

    render() {
        return (
            <div className="inputField">
                {localStorage.getItem('token') ?
                    <div>
                        <section className="avatar">
                            <Avatar
                                children={localStorage.getItem('username')[0]}
                            />
                        </section>
                        <section className="input">
                            <TextField
                                name="content"
                                hintText="댓글 추가..."
                                style={{fontFamily: 'main'}}
                                value={this.state.content}
                                onChange={this._handleChange}
                                onKeyUp={this._handleEnter}
                                fullWidth={true}
                            />
                        </section>
                        <p
                            className="submit_comment"
                            onClick={this._handleSubmit}
                        >
                            게시
                        </p>
                    </div>
                     :
                    <Link to="/login" style={{textDecoration: 'none'}}>
                        <ListItem
                            primaryText="댓글 작성을 위해선 로그인이 필요합니다. "
                            style={{fontFamily: 'main'}}
                        />
                    </Link>
                }

            </div>
        );
    }
}


export default CommentInput;