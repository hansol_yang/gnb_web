import React from 'react';
import {ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconRemove from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';


const Comment = ({comment, handleDelete}) => (
    <ListItem
        leftAvatar={
            <Avatar>
                {comment.username[0]}
            </Avatar>
        }
        style={{fontFamily: 'main'}}
        primaryText={comment.username}
        secondaryText={comment.content}
        rightIconButton={
            localStorage.getItem('username') === comment.username ?
                <IconButton
                    onClick={() => handleDelete(comment._id)}
                >
                    <IconRemove/>
                </IconButton>
                : undefined
        }
    />
);


export default Comment;