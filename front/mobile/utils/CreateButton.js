import React from 'react';
import FloatingButton from 'material-ui/FloatingActionButton';
import IconCreate from 'material-ui/svg-icons/content/create';


const CreateButton = ({toggleFunc}) => (
    <FloatingButton
        className="createButton"
        backgroundColor="black"
        onTouchTap={() => toggleFunc(true)}
        style={{zIndex: '1000'}}
    >
        <IconCreate/>
    </FloatingButton>
);


export default CreateButton;