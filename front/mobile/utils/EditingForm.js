import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import SubmitButton from 'material-ui/RaisedButton';


class EditingForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            body: ''
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleUpdateRequest = this._handleUpdateRequest.bind(this);
        this._handleEnter = this._handleEnter.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleUpdateRequest() {
        this.props.updateRequest(this.props.data._id, this.state.title, this.state.body, this.props.data.option);
        this.props.toggleFunc();
    }

    _handleEnter(e) {
        if(e.keyCode === 13) {
            this._handleUpdateRequest();
        }
    }

    componentDidMount() {
        let data = this.props.data;

        this.setState({
            title: data.title,
            body: data.body
        })
    }

    render() {
        return (
            <Paper zDepth={1} className="FormWrapper edit">
                <TextField
                    className="postingForm"
                    name="title"
                    floatingLabelText="제목"
                    type="text"
                    fullWidth={true}
                    onChange={this._handleChange}
                    value={this.state.title}
                />
                <TextField
                    className="postingForm"
                    name="body"
                    floatingLabelText="내용"
                    type="text"
                    style={{marginBottom: '2rem'}}
                    rows={7}
                    multiLine={true}
                    fullWidth={true}
                    onChange={this._handleChange}
                    value={this.state.body}
                    onKeyUp={this._handleEnter}
                />
                <SubmitButton
                    label="작성"
                    labelColor="white"
                    labelStyle={{fontFamily: 'main'}}
                    backgroundColor="black"
                    fullWidth={true}
                    onClick={this._handleUpdateRequest}
                />
            </Paper>
        );
    }
}


export default EditingForm;