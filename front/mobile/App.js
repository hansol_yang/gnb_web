import React from 'react';
import {Route, Switch, BrowserRouter as Router, Redirect} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

import NavBar from './components/NavBar';
import Home from '../mobile/Home';
import About from '../mobile/About';
import Notice from '../mobile/Notice';
import Post from '../mobile/Post';
import Auth from '../mobile/Auth';
import Detail from '../mobile/Detail';


const history = createHistory();

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isPosting: false,
            isEditing: false,
            isDetail: false
        };

        this._handlePostingMode = this._handlePostingMode.bind(this);
        this._handleEditingMode = this._handleEditingMode.bind(this);
        this._handleDetailMode = this._handleDetailMode.bind(this);
        this._handleInitMode = this._handleInitMode.bind(this);
    }

    _handlePostingMode(status) {
        this.setState({
            isPosting: status
        })
    }

    _handleEditingMode(status) {
        this.setState({
            isEditing: status
        })
    }

    _handleDetailMode(status) {
        this.setState({
            isDetail: status
        })
    }

    _handleInitMode() {
        if (this.state.isEditing) {
            this.setState({
                isEditing: false
            })
        } else {
            this.setState({
                isPosting: false,
                isDetail: false
            })
        }
    }

    render() {
        return (
            <Router history={history}>
                <div>
                    <NavBar
                        IOStatus={this.state.isPosting | this.state.isEditing | this.state.isDetail}
                        initFunc={this._handleInitMode}
                    />
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/about" component={About}/>
                        <Route path="/notice" render={
                            () => <Notice
                                isPosting={this.state.isPosting}
                                handlePostingMode={this._handlePostingMode}
                            />
                        }/>
                        <Route path="/post" render={
                            (match) => <Post
                                isPosting={this.state.isPosting}
                                handlePostingMode={this._handlePostingMode}
                                match={match}
                            />
                        }/>
                        <Route strict path="/login" render={
                            () => (
                                localStorage.getItem('token') ?
                                    <Redirect to="/"/> :
                                    <Auth/>
                            )
                        }/>
                        <Route path="/detail" render={
                            (match) => <Detail
                                isEditing={this.state.isEditing}
                                isDetail={this.state.isDetail}
                                handleEditingMode={this._handleEditingMode}
                                handleDetailMode={this._handleDetailMode}
                                match={match}
                            />
                        }/>
                    </Switch>
                </div>
            </Router>
        );
    }
}


export default App;