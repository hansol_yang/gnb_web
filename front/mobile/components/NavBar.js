import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/svg-icons/navigation/menu';
import IconBack from 'material-ui/svg-icons/navigation/chevron-left';
import Link from 'react-router-dom/Link';
import Item from 'material-ui/MenuItem'

import SideBar from './SideBar';


class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sideBarOpen: false
        };

        this._handleSideBarToggle = this._handleSideBarToggle.bind(this);
        this._handleSideBarClose = this._handleSideBarClose.bind(this);
    }

    _handleSideBarToggle() {
        this.setState({
            sideBarOpen: !this.state.sideBarOpen
        })
    }

    _handleSideBarClose() {
        this.setState({
            sideBarOpen: false
        })
    }

    render() {
        let options = [
            {to: '/', text: '홈'},
            {to: '/about', text: '지앤비란?'},
            {to: '/notice', text: '공지 사항'},
            {to: '/post', text: '일반 게시판'}
        ];

        return (
            <div>
                <AppBar
                    style={
                        rootStyle
                    }
                    iconElementRight={
                        <div className="menuType">
                            <div className="mobile">
                                <IconButton onTouchTap={this._handleSideBarToggle}>
                                    <IconMenu/>
                                </IconButton>
                            </div>
                            <div className="computer">
                                {options.map((item, i) => (
                                    <Link to={item.to} className="link" key={i}>
                                        <Item className="element">
                                            {item.text}
                                        </Item>
                                    </Link>
                                ))}
                                {localStorage.getItem('token') ?
                                    <Link
                                        to="#"
                                        className="link"
                                        onClick={() => {
                                            if(confirm('로그아웃 하시겠습니까?')) {
                                                localStorage.clear();
                                                location.reload();
                                            }
                                        }}
                                    >
                                        <Item
                                            className="element"
                                        >
                                            로그아웃
                                        </Item>

                                    </Link> :
                                    <Link
                                        to="/login"
                                        className="link"
                                    >
                                        <Item
                                            className="element"
                                        >
                                            로그인
                                        </Item>
                                    </Link>
                                }
                            </div>
                        </div>

                    }
                    iconElementLeft={
                        this.props.IOStatus ?
                            <div>
                                <IconButton
                                    onTouchTap={this.props.initFunc}
                                >
                                    <IconBack/>
                                </IconButton>
                            </div> : undefined
                    }
                />
                <SideBar
                    open={this.state.sideBarOpen}
                    handleCloseFunc={this._handleSideBarClose}
                />
            </div>
        );
    }
}

const rootStyle = {
    backgroundColor: 'white'
};


export default NavBar;