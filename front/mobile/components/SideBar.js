import React from 'react';
import Drawer from 'material-ui/Drawer';
import Item from 'material-ui/MenuItem';
import IconHome from 'material-ui/svg-icons/action/home';
import IconAbout from 'material-ui/svg-icons/social/people';
import IconNotice from 'material-ui/svg-icons/action/announcement';
import IconPost from 'material-ui/svg-icons/action/view-list';
import IconAuth from 'material-ui/svg-icons/communication/vpn-key';
import IconSignOut from 'material-ui/svg-icons/action/exit-to-app';
import Link from 'react-router-dom/Link';


let options = [
    {to: '/', icon: <IconHome/>, text: '홈'},
    {to: '/about', icon: <IconAbout/>, text: '지앤비란?'},
    {to: '/notice', icon: <IconNotice/>, text: '공지 사항'},
    {to: '/post', icon: <IconPost/>, text: '일반 게시판'}
];

const mapMenuToSideBar = (handleCloseFunc) => {
    return options.map((item, i) => (
        <Link
            to={item.to}
            className="sideBarMenuLink"
            onClick={handleCloseFunc}
            key={i}
        >
            <Item
                leftIcon={item.icon}
                style={{fontFamily: 'main'}}
            >
                {item.text}
            </Item>
        </Link>
    ))
};

const SideBar = ({open, handleCloseFunc}) => (
    <Drawer
        open={open}
        docked={false}
        openSecondary={true}
        onRequestChange={handleCloseFunc}
    >
        {localStorage.getItem('token') ?
            <Item style={{textAlign: 'center', fontFamily: 'main'}}>
                {`안녕하세요. ${localStorage.getItem('username')} 님`}
            </Item> :
            undefined
        }

        {mapMenuToSideBar(handleCloseFunc)}
        {localStorage.getItem('token') ?
            <Item
                className="sideBarMenuLink"
                leftIcon={<IconSignOut/>}
                onClick={() => {
                    if (confirm('로그아웃 하시겠습니까?')) {
                        localStorage.clear();
                        handleCloseFunc();

                    }
                }}
                style={{fontFamily: 'main'}}
            >
                로그아웃
            </Item> :
            <Link
                to="/login"
                className="sideBarMenuLink"
                onClick={handleCloseFunc}
            >
                <Item
                    leftIcon={<IconAuth/>}
                    style={{fontFamily: 'main'}}
                >
                    로그인
                </Item>
            </Link>
        }
    </Drawer>
);


export default SideBar;