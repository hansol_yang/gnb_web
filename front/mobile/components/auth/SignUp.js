import React from 'react';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import SubmitButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';

import {signupRequest} from '../../../redux/actions/users/signup';


class SignUp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password1: '',
            password2: ''
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSignup = this._handleSignup.bind(this);
        this._handleEnter = this._handleEnter.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleSignup() {
        if(this.state.password1 === this.state.password2) {
            this.props.signupRequest(this.state.username, this.state.password1);
            this.props.handleAuthMode();
        } else {
            alert('비밀번호를 다시 확인해주세요.');
            this.setState({
                password1: '',
                password2: ''
            })
        }
    }

    _handleEnter(e) {
        if(e.keyCode === 13) {
            this._handleSignup();
        }
    }


    render() {
        return (
            <Paper zDepth={1} className="authFieldsWrapper">
                <TextField
                    floatingLabelText="이름"
                    name="username"
                    type="text"
                    onChange={this._handleChange}
                    value={this.state.username}
                    style={{fontFamily: 'main'}}
                    fullWidth={true}
                />
                <TextField
                    floatingLabelText="비밀번호"
                    floatingLabelStyle={{fontFamily: 'main'}}
                    name="password1"
                    type="password"
                    onChange={this._handleChange}
                    value={this.state.password1}
                    fullWidth={true}
                />
                <TextField
                    floatingLabelText="비밀번호 확인"
                    floatingLabelStyle={{fontFamily: 'main'}}
                    name="password2"
                    type="password"
                    onChange={this._handleChange}
                    value={this.state.password2}
                    style={{marginBottom: '2rem'}}
                    fullWidth={true}
                    onKeyUp={this._handleEnter}
                />
                <SubmitButton
                    label="회원가입"
                    labelStyle={{fontFamily: 'main', color: 'white'}}
                    backgroundColor="black"
                    style={{marginBottom: '2rem'}}
                    fullWidth={true}
                    onTouchTap={this._handleSignup}
                />
                <p>회원이신가요?&nbsp;</p>
                <p className="authModeSelector" onClick={this.props.handleAuthMode}>로그인</p>
            </Paper>
        );
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        signupRequest: (username, password) => dispatch(signupRequest(username, password))
    }
};

SignUp = connect(undefined, mapDispatchToProps)(SignUp);


export default SignUp