import React from 'react';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import SubmitButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';

import {signinRequest} from '../../../redux/actions/users/signin';


class SignIn extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSignin = this._handleSignin.bind(this);
        this._handleEnter = this._handleEnter.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleSignin() {
        this.props.signinRequest(this.state.username, this.state.password);
    }

    _handleEnter(e) {
        if(e.keyCode === 13) {
            this._handleSignin();
        }
    }

    render() {
        return (
            <Paper zDepth={1} className="authFieldsWrapper">
                <TextField
                    floatingLabelText="이름"
                    name="username"
                    type="text"
                    onChange={this._handleChange}
                    value={this.state.username}
                    style={{fontFamily: 'main'}}
                    fullWidth={true}
                />
                <TextField
                    floatingLabelText="비밀번호"
                    floatingLabelStyle={{fontFamily: 'main'}}
                    name="password"
                    type="password"
                    onChange={this._handleChange}
                    value={this.state.password}
                    style={{marginBottom: '2rem'}}
                    fullWidth={true}
                    onKeyUp={this._handleEnter}
                />
                <SubmitButton
                    label="로그인"
                    labelStyle={{fontFamily: 'main', color: 'white'}}
                    backgroundColor="black"
                    style={{marginBottom: '2rem'}}
                    fullWidth={true}
                    onClick={this._handleSignin}
                />
                <p>처음이신가요?&nbsp;</p>
                <p className="authModeSelector" onClick={this.props.handleAuthMode}>회원가입</p>
            </Paper>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        response: state.usersReducer.response
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        signinRequest: (username, password) => dispatch(signinRequest(username, password))
    }
};

SignIn = connect(mapStateToProps, mapDispatchToProps)(SignIn);


export default SignIn