import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs'
import {connect} from 'react-redux';
import Paper from 'material-ui/Paper';

import List from './utils/List';
import {readRecentsRequest} from '../redux/actions/articles/readRecents';
import {readAllByOptionRequest} from '../redux/actions/articles/readAllByOption';


let tabItemContainerStyle = {
    backgroundColor: 'white'
};

class Home extends React.Component {
    componentDidMount() {
        this.props.readAllByOptionRequest(1);
        this.props.readRecentsRequest();
    }

    render() {
        return (
            <div className="container">
                <Paper zDepth={1} className="wrapper home">
                    <section id="header">
                        <section className="background"></section>
                        <section className="text">
                            <p>지앤비</p>
                            <p>지앤비는 웹 프로그래밍 동아리입니다</p>
                        </section>
                    </section>
                    <section>
                        <Tabs
                            tabItemContainerStyle={tabItemContainerStyle}
                            inkBarStyle={{backgroundColor: 'black'}}

                        >
                            <Tab
                                label="공지 사항"
                                buttonStyle={{color: 'black'}}
                            >
                                {this.props.notices.map((data, i) => (
                                    <List
                                        data={data}
                                        key={i}
                                    />
                                ))}

                            </Tab>
                            <Tab
                                label="최근 게시물"
                                buttonStyle={{color: 'black'}}
                            >
                                {this.props.recents.map((data, i) => (
                                    <List
                                        data={data}
                                        key={i}
                                    />
                                ))}
                            </Tab>
                        </Tabs>
                    </section>
                </Paper>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        notices: state.articlesReducer.articles,
        recents: state.articlesReducer.recents
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readAllByOptionRequest: (option) => dispatch(readAllByOptionRequest(option)),
        readRecentsRequest: () => dispatch(readRecentsRequest())
    }
};

Home = connect(mapStateToProps, mapDispatchToProps)(Home);


export default Home;