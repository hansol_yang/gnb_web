import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import App from './mobile/App';
import reducer from './redux/reducers';


import '../back/public/stylesheets/style.css';
import '../back/public/stylesheets/home_container.css';
import '../back/public/stylesheets/auth.css';
import '../back/public/stylesheets/detail_container.css';
import '../back/public/stylesheets/about_container.css';


const rootElement = document.getElementById('root');

const store = createStore(reducer, applyMiddleware(thunk));

injectTapEventPlugin();

ReactDOM.render(
    <MuiThemeProvider>
        <Provider store={store}>
            <App/>
        </Provider>
    </MuiThemeProvider>,
    rootElement
);

/* For Hot Module Replacement */
if (module.hot) {
    module.hot.accept();
}