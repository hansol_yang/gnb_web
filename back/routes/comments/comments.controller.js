var Article = require('../../models/Article');


// 댓글 생성
// POST: /comments/:id
exports.createComment = function (req, res) {
    var content = req.body.content;
    var username = req.decoded.username;
    var id = req.params.id;

    var createComment = function (id) {

        var comment = {
            content: content,
            username: username
        };

        return Article.findOneAndUpdate({_id: id}, {$push: {comments: comment}}).exec();
    };

    var respond = function (article) {
        return res.json({
            msg: '댓글 생성에 성공했습니다.',
            code: 300
        })
    };

    var onError = function (err) {
        return res.json({
            msg: err.message
        });
    };

    createComment(id)
        .then(respond)
        .catch(onError);

};


// 댓글 삭제
// DELETE: /comments/:id
exports.deleteComment = function (req, res) {
    var id = req.params.id;
    var commentId = req.body.id;

    var findById = function (id) {
        return Article.findOne({_id: id}).exec();
    };

    var findCommentAndRemove = function (article) {
        var index = article.comments.findIndex(function (comment) {
            return String(comment._id) === commentId;
        });

        article.comments.splice(index, 1);

        article.save(function (err) {
            if (err) throw(err);
            return res.json({
                msg: '댓글 삭제에 성공했습니다.',
                code: 300
            })
        })
    };

    var onError = function (err) {
        return res.json({
            msg: err.message
        })
    };

    findById(id)
        .then(findCommentAndRemove)
        .catch(onError);
};