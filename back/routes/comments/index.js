var router = require('express').Router();

var controller = require('./comments.controller');
var auth = require('../auth');


router.post('/:id', auth);
router.post('/:id', controller.createComment);

router.delete('/:id', controller.deleteComment);


module.exports = router;