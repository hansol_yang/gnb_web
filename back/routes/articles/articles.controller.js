var Article = require('../../models/Article');


// 게시물 생성
// POST: /articles
// BODY: {title, body, option}
// body의 option은 0(일반 게시물)이 default, 1(공지사항)

// ERRORS
// 0: 잘못된 게시물 타입입니다.
// 1: 게시물 생성 권한이 없습니다.
exports.createArticle = function (req, res) {
    // 토큰에 문제가 있으면 이 함수가 실행되지 않는다.
    var username = req.decoded.username;
    var isAdmin = req.decoded.isAdmin;
    var {title, body, option} = req.body;

    var _create = new Promise(function (resolve, reject) {
        function createNew () {
            var article = new Article({
                title: title,
                body: body,
                username: username,
                option: option
            });

            article.save(function (err) {
                if (err) reject(err);
                resolve(article);
            });
        }

        switch (option) {
            case 0:
                createNew();
                break;
            case 1:
                if(isAdmin === false) {
                    return res.json({
                        msg: '게시물 생성 권한이 없습니다.',
                        code: 1
                    })
                } else {
                    createNew();
                    break;
                }
            default:
                return res.json({
                    msg: '잘못된 게시물 타입입니다.',
                    code: 1
                })
        }

    });

    var _respond = function (article) {
        switch (article.option) {
            case 0:
                return res.json({
                    msg: '일반 게시물 생성에 성공했습니다.',
                    code: 300,
                    article: article
                });

            case 1:
                return res.json({
                    msg: '공지 사항 생성에 성공했습니다.',
                    code: 300,
                    article: article
                });

            default:
                throw(new Error('예기치 못한 오류 발생'));
        }
    };

    var _onError = function (err) {
        return res.json({
            msg: err.message
        })
    };

    _create
        .then(_respond)
        .catch(_onError);
};


// 게시물 전체 조회 -> 최근 게시물을 위한 API
// GET: /articles/recents
exports.readRecentsArticles = function (req, res) {
    var _readAll = function () {
        return Article.find({}).sort({createdAt: -1}).limit(3).exec();
    };

    var _respond = function (articles) {
        return res.json({
            msg: '최근 게시물 조회에 성공했습니다.',
            code: 300,
            recents: articles
        })
    };

    var _onError = function (err) {
        return res.json({
            msg: err.message
        });
    };

    _readAll()
        .then(_respond)
        .catch(_onError);
};


// 게시물 전체 조회 -> 옵션별 전체 조회를 위한 API
// GET: /articles/option/:option
exports.readArticlesByOption = function(req, res) {
    var option = req.params.option;

    var _read = function(option) {
        return Article.find({option: option}).exec();
    };

    var _response = function(articles) {
        return res.json({
            msg: '옵션별 게시물 조회에 성공했습니다.',
            code: 300,
            articles: articles
        });
    };

    var _onError = function(err) {
        return res.json({
            msg: err.message
        });
    };

    _read(option)
        .then(_response)
        .catch(_onError);
};

// 게시물 특정 조회
// GET: /articles/:id
exports.readArticle = function (req, res) {
    var id = req.params.id;

    var _read = function (id) {
        return Article.findOne({_id: id}).exec();
    };

    var _respond = function (article) {
        return res.json({
            msg: '게시물 조회에 성공했습니다.',
            code: 300,
            article: article
        });
    };

    var _onError = function (err) {
        return res.json({
            msg: err.message
        });
    };

    _read(id)
        .then(_respond)
        .catch(_onError);
};


// 게시물 수정
// GET: /articles/:id
// ERRORS
// 0: 권한이 없습니다.
// 404: 해당 게시물이 존재하지 않습니다.
exports.updateArticle = function (req, res) {
    var {title, body, option} = req.body;
    var id = req.params.id;
    var username = req.decoded.username;

    var _findById = function (id) {
        return Article.findOne({_id: id}).exec();
    };

    var _update = function (article) {
        if (article) {
            if (article.username !== username) {
                return res.json({
                    msg: '권한이 없습니다.',
                    code: 0
                });
            } else {
                article.title = title;
                article.body = body;
                article.option = option;
                article.save(function (err, article) {
                    if (err) throw err;
                    return res.json({
                        msg: '게시물 수정에 성공했습니다.',
                        code: 300,
                        article: article
                    })
                })
            }
        } else {
            return res.json({
                msg: '해당 게시물이 존재하지 않습니다.',
                code: 404
            })
        }

    };

    var _onError = function (err) {
        return res.json({
            msg: err.message
        });
    };

    _findById(id)
        .then(_update)
        .catch(_onError);
};


// 게시물 삭제
// DELETE: /articles/:id

// ERRORS
// 0: 권한이 없습니다.
// 404: 해당 게시물이 존재하지 않습니다.
exports.deleteArticle = function (req, res) {
    var id = req.params.id;
    var username = req.decoded.username;

    var _findById = function (id) {
        return Article.findOne({_id: id}).exec();
    };

    var _delete = function (article) {
        if (article) {
            if (article.username !== username) {
                return res.json({
                    msg: '권한이 없습니다.',
                    code: 0
                });
            } else {
                article.remove(function (err) {
                    if (err) throw(err);

                    return res.json({
                        msg: '게시물 삭제에 성공했습니다.',
                        code: 300
                    })
                });
            }
        } else {
            return res.json({
                msg: '해당 게시물이 존재하지 않습니다.',
                code: 404
            })
        }

    };

    var _onError = function (err) {
        return res.json({
            msg: err.message
        });
    };

    _findById(id)
        .then(_delete)
        .catch(_onError);
};