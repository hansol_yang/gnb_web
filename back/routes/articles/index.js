var router = require('express').Router();

var controller = require('./articles.controller');
var auth = require('../auth');


router.post('/', auth);
router.post('/', controller.createArticle);

router.get('/recents', controller.readRecentsArticles);

router.get('/:id', controller.readArticle);

router.get('/option/:option', controller.readArticlesByOption);

router.put('/:id', auth);
router.put('/:id', controller.updateArticle);

router.delete('/:id', auth);
router.delete('/:id', controller.deleteArticle);

module.exports = router;