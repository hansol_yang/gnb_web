var User = require('../../models/User');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');


// ERROR CODE
// 0: 이미 등록된 이름입니다.

exports.signup = function (req, res) {
    // SIGN_UP
    // POST: /users/signup
    // BODY: {username, password}

    var data = req.body;

    var findByUsername = function (username) {
        return User.findOne({username: username}).exec(); // return <Promise>
    };

    var createNewUser = function (user) {
        if (user) {
            return res.json({
                msg: '이미 등록된 이름입니다.',
                code: 0
            });
        }

        else {
            bcrypt.hash(data.password, 10, function (err, hash) {
                if (err) throw(err);
                var user = new User({
                    username: data.username,
                    password: hash
                });

                user.save(function (err) {
                    if (err) throw(err);

                    return res.json({
                        msg: '회원가입에 성공하였습니다',
                        code: 300
                    })
                });
            })
        }
    };

    var onError = function (err) {
        return res.json({
            msg: err.message,
            code: res.statusCode
        })
    };

    findByUsername(data.username)
        .then(createNewUser)
        .catch(onError);
};


// ERRORS
// 1: 회원 정보를 다시 확인해주세요.

exports.signin = function (req, res) {
    // SIGNIN
    // POST: /users/signin
    // BODY: {username, password}

    var data = req.body;
    var secret = req.app.get('jwt-secret');

    var findByUsername = function (username) {
        return User.findOne({username: username}).exec();
    };

    var verify = function (user) {
        if (!user) return res.json({
            msg: '회원 정보를 다시 확인해주세요.',
            code: 1
        });

        bcrypt.compare(data.password, user.password, function (err, match) {
            if (err) throw(err);

            if (!match) {
                return res.json({
                    msg: '회원 정보를 다시 확인해주세요.',
                    code: 1
                });
            } else {
                jwt.sign(
                    {
                        _id: user._id,
                        username: user.username,
                        isAdmin: user.isAdmin
                    },
                    secret,
                    {
                        expiresIn: '7d',
                        issuer: 'www.gnb.com',
                        subject: 'userInfo'
                    },
                    function (err, token) {
                        if (err) throw(err);
                        return res.json({
                            msg: '로그인에 성공했습니다.',
                            code: 300,
                            token: token,
                            isAdmin: user.isAdmin,
                            username: user.username
                        })
                    }
                )
            }
        })
    };

    var onError = function (err) {
        return res.json({
            msg: err.message
        });
    };

    findByUsername(data.username)
        .then(verify)
        .catch(onError);
};