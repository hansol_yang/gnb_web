var router = require('express').Router();

var User = require('./users');
var Article = require('./articles');
var Comment = require('./comments');


router.use('/users', User);
router.use('/articles', Article);
router.use('/comments', Comment);


module.exports = router;
