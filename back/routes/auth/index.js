var jwt = require('jsonwebtoken');


// 회원 검증 미들웨어
// ERRORS
// 1) TokenExpiredError: jwt expired -> error {name: 'TokenExpiredError', 'message': 'jwt expired', expiredAt} -> decoded가 undefined로 출력됨
module.exports = function (req, res, next) {
    var token = req.headers['x-access-token'];
    var secret = req.app.get('jwt-secret');

    var verify = new Promise(function (resolve, reject) {
        jwt.verify(token, secret, function (err, decoded) {
            if (err) reject(err);
            resolve(decoded);
        })
    });

    var onError = function (err) {
        if (err.name === 'TokenExpiredError')
            return res.json({
                msg: '토큰이 만료되었습니다'
            });
        else if (err.name === 'JsonWebTokenError')
            return res.json({
                msg: `토큰에 문제가 있습니다.\n${err.message}`
            });
        else
            return res.json({
                msg: err.message
            });
    };

    verify
        .then(function(decoded) {
            req.decoded = decoded;
            next();
        })
        .catch(onError);
};