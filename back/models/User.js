var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var User = new Schema({
    username: String,
    password: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    editedAt: {
        type: Date,
        default: Date.now
    },
    isEdit: {
        type: Boolean,
        default: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
});

var users = mongoose.model('user', User);


module.exports = users;