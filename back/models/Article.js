var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var Article = new Schema({
    title: {
        type: String,
        default: '제목 없음'
    },
    body: {
        type: String,
        default: '내용 없음'
    },
    username: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    editedAt: {
        type: Date,
        default: Date.now
    },
    isEdited: {
        type: Boolean,
        default: false
    },
    option: Number,
    comments: [
        {
            content: String,
            username: String,
            createdAt: {
                type: Date,
                default: Date.now
            }
        }
    ]
});

var articles = mongoose.model('article', Article);


module.exports = articles;